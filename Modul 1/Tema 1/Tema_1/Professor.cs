﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_1
{
    class Professor : Person
    {
        private bool _wrongData = false;
        private string _professionalExperience;
        private string _salary;
        private string _profile;

        public Professor()
        {
            ReadData();
        }

        public override void ReadData()
        {
            Console.Write("Introduce professor data: " + Environment.NewLine + "First name: ");
            firstName = Console.ReadLine();
            Console.Write("Last name: ");
            lastName = Console.ReadLine();
            Console.Write("Age: ");
            age = Console.ReadLine();
            Console.Write("City: ");
            city = Console.ReadLine();
            Console.Write("Professional experience [years]: ");
            _professionalExperience = Console.ReadLine();
            Console.Write("Salary: ");
            _salary = Console.ReadLine();
            Console.Write("Desired job name: ");
            _profile = Console.ReadLine();
            VerifyData();
        }

        public override void VerifyData()
        {
            string errorMessage = "";
            if (firstName == "" || firstName.Length < 3)
            {
                errorMessage += "Invalid first name; ";
            }

            if (lastName == "" || lastName.Length < 3)
            {
                errorMessage += "Invalid last name; ";
            }

            if (age == "" || Convert.ToInt32(age) < 18)
            {
                errorMessage += "Invalid age, must be 18+; ";
            }

            if (city == "" || city.Length < 3)
            {
                errorMessage += "Invalid city; ";
            }

            if (_professionalExperience == "" || Convert.ToInt32(_professionalExperience) < 1)
            {
                errorMessage += "Invalid professional experience, must be 1+; ";
            }

            if (_salary == "" || Convert.ToInt32(_salary) < 1)
            {
                errorMessage += "Invalid salary, must be 1+; ";
            }

            if (_profile == "" || _profile.Length < 2)
            {
                errorMessage += "Invalid job name; ";
            }

            if (errorMessage != "")
            {
                _wrongData = true;
                Console.WriteLine(errorMessage);
            }
        }

        public override string ToString()
        {
            return string.Format("[First name]: " + firstName + " " + "[Last name]: " + lastName + " " + "[Age]: " + age + " " + "[Professional experience [years]]: " + _professionalExperience + " " + "[Salary]: " + _salary + " " + "[Desired job name]: " + _profile);
        }

        public bool WrongData()
        {
            return _wrongData;
        }
    }
}