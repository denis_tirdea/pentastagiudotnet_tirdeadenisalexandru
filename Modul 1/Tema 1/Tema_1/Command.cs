﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_1
{
    public enum Command
    {
        Exit,
        AddStudent,
        AddProfessor,
        ShowAllPersons,
        ShowStudents,
        ShowProfessors,
        AddStudentToProfessor,
        ShowStudentToProfessors
    }
}