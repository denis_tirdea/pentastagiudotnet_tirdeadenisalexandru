﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_1
{
    abstract class Person
    {
        protected string firstName { get; set; }
        protected string lastName { get; set; }
        protected string age { get; set; }
        protected string city { get; set; }

        abstract public void ReadData();

        abstract public void VerifyData();
    }
}