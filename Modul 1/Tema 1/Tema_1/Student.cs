﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_1
{
    class Student:Person
    {
        private string _grade;
        private string _profile;
        private bool _wrongData = false;

        public Student()
        {
            ReadData();
        }

        public override void ReadData()
        {
            Console.Write("Introduce student data: " + Environment.NewLine + "First name: ");
            firstName = Console.ReadLine();
            Console.Write("Last name: ");
            lastName = Console.ReadLine();
            Console.Write("Age: ");
            age = Console.ReadLine();
            Console.Write("City: ");
            city = Console.ReadLine();
            Console.Write("High school average grade: ");
            _grade = Console.ReadLine();
            Console.Write("Desired profile: ");
            _profile = Console.ReadLine();
            VerifyData();
        }

        public override void VerifyData()
        {
            string errorMessage = "";
            if (firstName == "" || firstName.Length < 3)
            {
                errorMessage+="Invalid first name; ";
            }

            if (lastName == "" || lastName.Length < 3)
            {
                errorMessage += "Invalid last name; ";
            }

            if (age == "" || Convert.ToInt32(age) < 18)
            {
                errorMessage += "Invalid age, must be 18+; ";
            }

            if (city == "" || city.Length < 3)
            {
                errorMessage += "Invalid city; ";
            }

            if (_grade == "" || Convert.ToInt32(_grade) < 1)
            {
                errorMessage += "Invalid grade, must be 1+; ";
            }

            if (_profile == "" || _profile.Length < 1)
            {
                errorMessage += "Invalid profile; ";
            }

            if (errorMessage != "")
            {
                _wrongData = true;
                Console.WriteLine(errorMessage);
            }
        }

        public bool WrongData()
        {
            return _wrongData;
        }

        public override string ToString()
        {
            return string.Format("[First name]: " + firstName + " " + "[Last name]: " + lastName + " " + "[Age]: " + age + " " + "[City]: " + city + " " + "[High school average grade]: " + _grade + " " + "[Desired profile]: " + _profile );
        }
    }
}