﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_1
{
    class Program 
    {
        private int _studentID, _professorID;
        Student student;
        Professor professor;
        private int _chooseOption;
        List<string> StudentList = new List<string>();
        List<string> ProfessorList = new List<string>();
        List<string> StudentsToProfessors = new List<string>();

        public Program()
        {
            do
            {
                Console.WriteLine("1. Add student");
                Console.WriteLine("2. Add professor");
                Console.WriteLine("3. Show all persons");
                Console.WriteLine("4. Show students");
                Console.WriteLine("5. Show professors");
                Console.WriteLine("6. Add student to professor");
                Console.WriteLine("7. Show all students from professors");
                Console.WriteLine("0. Exit");

                _chooseOption = Convert.ToInt32(Console.ReadLine());
                Command switchCommand = (Command)_chooseOption;
                switch (switchCommand)
                {
                    case Command.Exit:
                        return;
                    case Command.AddStudent:
                        AddStudent();
                        break;
                    case Command.AddProfessor:
                        AddProfessor();
                        break;
                    case Command.ShowAllPersons:
                        DisplayPersons(StudentList, ProfessorList);
                        break;
                    case Command.ShowStudents:
                        Console.WriteLine("Show students: ");
                        DisplayPersons(StudentList); 
                        break;
                    case Command.ShowProfessors:
                        Console.WriteLine("Show professors: ");
                        DisplayPersons(ProfessorList); 
                        break;
                    case Command.AddStudentToProfessor:
                        AddStudentToProfessor();
                        break;
                    case Command.ShowStudentToProfessors:
                        DisplayPersons(StudentsToProfessors); 
                        break;
                    default:
                        Console.WriteLine("Invalid option");
                        break;
                }
            }
            while (true);
        }

        public void AddStudent()
        {
            student = new Student();
            if (student.WrongData() != true)
            {
                StudentList.Add("[ID]: " + _studentID + " " + student.ToString());
                _studentID++;
            }
        }

        public void AddProfessor()
        {
            professor = new Professor();
            if (professor.WrongData() != true)
            {
                ProfessorList.Add("[ID]: " + _professorID + " " + professor.ToString());
                StudentsToProfessors.Add("[ID]: " + _professorID + " " + professor.ToString());
                _professorID++;
            }
        }

        public void DisplayPersons(List<string> AllPersons)
        {
            foreach (string person in AllPersons)
            {
                Console.WriteLine(person);
            }
            Console.WriteLine();
        }

        public void DisplayPersons(List<string> StudentList, List<string> ProfessorList)
        {
            Console.WriteLine("Display students: ");
            foreach (string student in StudentList)
            {
                Console.WriteLine(student);
            }
            Console.WriteLine("Display professors: ");
            foreach (string professor in ProfessorList)
            {
                Console.WriteLine(professor);
            }
            Console.WriteLine();
        }

        public void AddStudentToProfessor()
        {
            int studentID;
            int professorID;

            DisplayPersons(StudentList, ProfessorList);
            Console.Write("Introduce student ID: ");
            studentID = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce desired professor ID: ");
            professorID = Convert.ToInt32(Console.ReadLine());

            if (StudentList.Count == 0 || ProfessorList.Count == 0)
            {
                Console.WriteLine("You must introduce at least one student and one professor" + Environment.NewLine);
            }
            else if (studentID >= 0 && professorID >= 0)
            {
                string professor = StudentsToProfessors.ElementAt<string>(professorID);
                StudentsToProfessors.RemoveAt(professorID);
                StudentsToProfessors.Insert(professorID, professor + Environment.NewLine + "      Student: " + StudentList.ElementAt<string>(studentID));
            }
            else
            {
                Console.WriteLine("ID must be >= 0" + Environment.NewLine);
            }
        }

        static void Main(string[] args)
        {
            Program program = new Program();
        }
    }
}