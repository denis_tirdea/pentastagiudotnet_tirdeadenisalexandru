﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2
{
    class Puncte
    {
        public Puncte()
        {
            Puncte_xy p = new Puncte_xy();
            p.Run();
        }
    }
    struct Puncte_xy
    {
        int x;
        int y;
        public event EventHandler exEvent;
        public void Run() {
            exEvent += new EventHandler(this.Handler1);
            exEvent += new EventHandler(this.Handler2);
            exEvent += new EventHandler(this.Handler3);
            CitesteCoordonate();
        }

        public void CitesteCoordonate()
        {
            Console.WriteLine("Punct 1:");
            var punct1 = new Puncte_xy { x = Citeste_X(), y = Citeste_Y() };
            Console.WriteLine("Punct 2:");
            var punct2 = new Puncte_xy { x = Citeste_X(), y = Citeste_Y() };
            DeclanseazaEveniment(punct1, punct2);
        }

        public void DeclanseazaEveniment(Puncte_xy p1, Puncte_xy p2)
        {
            if (exEvent != null)
            {
                Console.WriteLine("Eveniment declansat!");
            }
            CalculeazaDistanta(p1, p2);
        }

        public void CalculeazaDistanta(Puncte_xy p1, Puncte_xy p2)
        {
            double distanta = Math.Sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
            Console.WriteLine("Distanta dintre puncte este: " + distanta);
        }

        public void Handler1(object sender, EventArgs e)
        {
            Console.WriteLine("Test1");
        }

        public void Handler2(object sender, EventArgs e)
        {
            Console.WriteLine("Test2");
        }

        public void Handler3(object sender, EventArgs e)
        {
            Console.WriteLine("Test3");
        }

        public int Citeste_X()
        {
            int temp;
            Console.Write("Introduceti x = ");
            temp = Convert.ToInt32(Console.ReadLine());
            return temp;
        }

        public int Citeste_Y()
        {
            int temp;
            Console.Write("Introduceti y = ");
            temp = Convert.ToInt32(Console.ReadLine());
            return temp;
        }
    }
}