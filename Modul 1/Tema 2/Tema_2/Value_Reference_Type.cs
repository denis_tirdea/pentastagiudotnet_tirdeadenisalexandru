﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2
{
    class Value_Reference_Type
    {
        public Value_Reference_Type()
        {
            ValueType v = new ValueType();
            ReferenceType rt = new ReferenceType();
        }
    }
    class ValueType
    {
        private int valueType;
        public ValueType()
        {
            valueType = 255;
            var refType = new Person { Name = "Denis" };
            Console.WriteLine("Pentru ValueType: ");
            Console.WriteLine("Valori initiale : {0} {1} ", valueType, refType.Name);
            ChangeValues(valueType, refType);
            Console.WriteLine("Valori schimbate : {0} {1} // In acest caz numele se schimba deoarece clasa insasi este o referinta, dar nu se inlocuieste obiectul", valueType, refType.Name);
        }
        public void ChangeValues(int valueType, Person person)
        {
            valueType = 500;
            person.Name = "Alexandru";
        }
    }
    class ReferenceType
    {
        private int valueType;
        public ReferenceType()
        {
            valueType = 255;
            var refType = new Person { Name = "Denis" };
            Console.WriteLine("Pentru ReferenceType: ");
            Console.WriteLine("Valori initiale : {0} {1} ", valueType, refType.Name);
            ChangeValues(ref valueType,ref refType);
            Console.WriteLine("Valori schimbate : {0} {1} ", valueType, refType.Name);
        }
        public void ChangeValues(ref int valueType, ref Person person)
        {
            valueType = 500;
            person.Name = "Alexandru";
        }
    }
    class Person
    {
        public string Name { get; set; }
    }
}