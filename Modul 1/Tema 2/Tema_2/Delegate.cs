﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2
{
    class Delegate
    {
        public int x, y;
        private string _optiune;
        public delegate void ex_Delegate(int x, int y);
        public Delegate()
        {
            ex_Delegate del;
            Citeste_X_si_Y();
            do
            {
                Console.WriteLine("     1. Adunare" + Environment.NewLine + "     2. Scadere" + Environment.NewLine + "     3. Inmultire" + Environment.NewLine + "     4. Impartire" + Environment.NewLine + "     5. Ridicare la putere" + Environment.NewLine + "     6. Return MENIU");
                _optiune = Convert.ToString(Console.ReadLine());
                Console.Write("Rezultat: ");
                switch (_optiune)
                {
                    case "1":
                        del = Aduna;
                        del.Invoke(x, y);
                        break;
                    case "2":
                        del = Scade;
                        del.Invoke(x, y);
                        break;
                    case "3":
                        del = Inmultire;
                        del.Invoke(x, y);
                        break;
                    case "4":
                        del = Impartire;
                        del.Invoke(x, y);
                        break;
                    case "5":
                        del = Ridicare_la_putere;
                        del.Invoke(x, y);
                        break;
                    case "6":
                        _optiune = "0";
                        break;
                    default:
                        Console.WriteLine("Optiune invalida");
                        break;
                }
            }
            while (_optiune != "0");
        }
        public void Citeste_X_si_Y()
        {
            Console.Write("Introduceti x = ");
            x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduceti y = ");
            y = Convert.ToInt32(Console.ReadLine());
        }

        public void Aduna(int x, int y)
        {
            Console.WriteLine(x + y);
        }

        public void Scade(int x, int y)
        {
            Console.WriteLine(x - y);
        }

        public void Inmultire(int x, int y)
        {
            Console.WriteLine(x * y);
        }

        public void Impartire(int x, int y)
        {
            Console.WriteLine((float)x / (float)y);
        }

        public void Ridicare_la_putere(int x, int y)
        {
            Console.WriteLine(Math.Pow(x,y));
        }
    }
}