﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Tema_3
{
    class LoadBalancer
    {
        private static object syslock = new object();
        private static LoadBalancer _instance;

        public void StartFirstThread(string htmlPage)
        {
            Thread thread = new Thread(() => ProcessHtmlPage(htmlPage));
            thread.Start();
        }

        public void StartSecondThread(string htmlPage)
        {
            Thread thread = new Thread(() => ProcessHtmlPage(htmlPage));
            thread.Start();
        }

        public static LoadBalancer Load()
        {
            lock (syslock)
            {
                if (_instance == null)
                {
                    _instance = new LoadBalancer();
                }
            }
            return _instance;
        }

        public void ProcessHtmlPage(string htmlPage)
        {
            var sw = Stopwatch.StartNew();
            Console.WriteLine("Thread {0}: {1}, Priority {2}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.ThreadState, Thread.CurrentThread.Priority);
            Client client = new Client();
            Server server = new ConcreteHTMLPage();
            client.ReadHTMLPage(server, htmlPage);
            HTMLPage htmlpage = server.ShowHtmlPage();
            Console.WriteLine("Thread {0}: Elapsed {1:N2} seconds", Thread.CurrentThread.ManagedThreadId, sw.ElapsedMilliseconds / 1000.0);
        }
    }
}