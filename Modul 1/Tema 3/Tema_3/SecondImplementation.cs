﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Tema_3
{
    class SecondImplementation : Processor
    {
        public override void FirstStep()
        {
            inputValue = (int)Math.Pow(inputValue, 2);
        }

        public override void SecondStep()
        {
            inputValue -= 20;
        }

        public override void ThirdStep()
        {
            File.AppendAllText("numbers.txt", inputValue.ToString() + Environment.NewLine);
        }
    }
}