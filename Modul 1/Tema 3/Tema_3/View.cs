﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    class View
    {
        private int _chooseOption;
        Controller controller = new Controller();

        public View()
        {
            do
            {
                Console.WriteLine(Environment.NewLine + "1. Download net pages content");
                Console.WriteLine("2. Process input numbers from keyboard");
                Console.WriteLine("3. Coffee maker");
                Console.WriteLine("0. Exit");

                _chooseOption = Convert.ToInt32(Console.ReadLine());
                Command command = (Command)_chooseOption;
                switch (command)
                {
                    case Command.Exit:
                        return;
                    case Command.DownloadNetPages:
                        DownloadNetPages();
                        break;
                    case Command.ProcessInputNumbers:
                        controller.ProcessInputNumbers();
                        break;
                    case Command.CoffeeMaker:
                        CoffeeMakerMachine();
                        break;
                    default:
                        Console.WriteLine("Invalid option");
                        break;
                }
            }
            while (true);
        }

        public void DownloadNetPages()
        {
            Console.WriteLine("     1.    Read one html page");
            Console.WriteLine("     2.    Read more html pages");
            Console.WriteLine("     0.    Exit");

            _chooseOption = Convert.ToInt32(Console.ReadLine());
            Command command = (Command)_chooseOption;
            switch (command)
            {
                case Command.ReadOnePage:
                    controller.ReadOnePage();
                    break;
                case Command.ReadMorePages:
                    controller.ReadMorePages();
                    break;
                case Command.Exit:
                    return;
            }
        }

        public void CoffeeMakerMachine()
        {
            bool selectedOption;
            int coffeeID;
            int[] ingredientsID = new int[2];
            string inputKeyboard;

            do
            {
                Console.WriteLine("     Select your desired coffee: ");
                Console.WriteLine("     1.    Espresso");
                Console.WriteLine("     2.    Espresso italiano");
                Console.WriteLine("     3.    Cappuccino");
                Console.WriteLine("     4.    Moccacino");
                Console.WriteLine("     5.    Classic coffee");
                selectedOption = int.TryParse(inputKeyboard = Console.ReadLine(), out coffeeID);
            }
            while (!selectedOption);

            do
            {
                Console.WriteLine("          Select 2 ingredients for your coffee: ");
                Console.WriteLine("          1.    Milk");
                Console.WriteLine("          2.    Sugar");
                Console.WriteLine("          3.    Honey");
                selectedOption = int.TryParse(inputKeyboard = Console.ReadLine(), out ingredientsID[0]);
                selectedOption = int.TryParse(inputKeyboard = Console.ReadLine(), out ingredientsID[1]);
            }
            while (!selectedOption);

            controller.CoffeeMaker(coffeeID, ingredientsID);
        }
    }
}