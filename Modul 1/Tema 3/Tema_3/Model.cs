﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    class Model
    {
        public bool CheckWebPage(string page)
        {
            if (page.Length >= 5)
            {
                return true;
            }
            return false;
        }

        public bool CheckNumber(string inputNuber)
        {
            bool isInteger = int.TryParse(inputNuber, out int number);
            return isInteger;
        }

        public bool CheckCoffeeID(int inputNumber)
        {
            if (inputNumber > 0 && inputNumber < 6)
            {
                return true;
            }
            return false;
        }
    }
}