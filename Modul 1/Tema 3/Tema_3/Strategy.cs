﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    public abstract class Strategy
    {
        public abstract void SaveHTMLPage(string htmlPage);
    }

    public class Context
    {
        private Strategy _strategy;

        public Context(Strategy strategy)
        {
            this._strategy = strategy;
        }

        public void ContextAlgorithm(string htmlPage)
        {
            _strategy.SaveHTMLPage(htmlPage);
        }
    }
}