﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    abstract class Server
    {
        public abstract void ReadHtmlPage(string page);

        public abstract HTMLPage ShowHtmlPage();
    }
}