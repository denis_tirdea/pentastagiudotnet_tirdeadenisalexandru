﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    abstract class Processor
    {
        static double inputNumber = 0;
        public Processor()
        {
            this.FirstStep();
            this.SecondStep();
            this.ThirdStep();
        }

        public static double inputValue
        {
            get { return inputNumber; }
            set { inputNumber = value; }
        }

        abstract public void FirstStep();

        abstract public void SecondStep();

        abstract public void ThirdStep();
    }
}