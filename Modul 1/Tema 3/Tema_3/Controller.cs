﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    class Controller
    {
        Model model = new Model();
        public void ReadOnePage()
        {
            string page;
            Model model = new Model();
            LoadBalancer loadbalancer = LoadBalancer.Load();

            Console.WriteLine("     Please, introduce your net page: ");
            page = Console.ReadLine();
            model.CheckWebPage(page);

            if (model.CheckWebPage(page))
            {
                loadbalancer.StartFirstThread(page);
            }
            else
            {
                Console.WriteLine("Invalid page");
                return;
            }

            Console.ReadKey();
        }

        public void ReadMorePages()
        {
            string page;
            Model model = new Model();
            LoadBalancer loadbalancer = LoadBalancer.Load();
            List<string> htmlPages = new List<string>();

            do
            {
                Console.WriteLine("     Please, introduce your net pages[0 for exit]: ");
                page = Console.ReadLine();
                model.CheckWebPage(page);

                if (model.CheckWebPage(page) && page != "0")
                {
                    htmlPages.Add(page);
                }
                else if (page == "0")
                {
                    Console.WriteLine("Processing all pages...");
                }
                else
                {
                    Console.WriteLine("Invalid page");
                }
            }
            while (page != "0");

            for (int counter = 0; counter <= htmlPages.Count - 1; counter += 2)
            {
                loadbalancer.StartFirstThread(htmlPages[counter]);
                if (counter + 1 < htmlPages.Count)
                {
                    loadbalancer.StartSecondThread(htmlPages[counter + 1]);
                }

                Console.ReadKey();
            }
        }

        public void ProcessInputNumbers()
        {
            string inputNumber;
            Model model = new Model();
            do
            {
                Console.Write("Please, introduce your number [0 - for exit]: ");
                inputNumber = Console.ReadLine();

                if (!model.CheckNumber(inputNumber))
                {
                    Console.WriteLine("Invalid input number");
                }
                else if (inputNumber == "0")
                {
                    return;
                }
                else
                {
                    Processor[] processor = new Processor[2];
                    Processor.inputValue = Convert.ToInt32(inputNumber);
                    processor[0] = new FirstImplementation();
                    Processor.inputValue = Convert.ToInt32(inputNumber);
                    processor[1] = new SecondImplementation();
                }
            }
            while (inputNumber != "0");
        }

        public void CoffeeMaker(int coffeeID, int[] ingredientsID)
        {
            if (model.CheckCoffeeID(coffeeID))
            {
                Coffee coffee = new Coffee(coffeeID);
                Milk milk = new Milk();
                Sugar sugar = new Sugar();
                Honey honey = new Honey();
                List<int> ingredients = new List<int>();

                ingredients.Add(ingredientsID[0]);
                ingredients.Add(ingredientsID[1]);

                if (ingredients.Contains(1) && ingredients.Contains(2))
                {
                    milk.AddComponent(coffee);
                    sugar.AddComponent(milk);
                    sugar.Operation();
                }
                else if (ingredients.Contains(1) && ingredients.Contains(3))
                {
                    milk.AddComponent(coffee);
                    honey.AddComponent(milk);
                    honey.Operation();
                }
                else if (ingredients.Contains(2) && ingredients.Contains(3))
                {
                    sugar.AddComponent(coffee);
                    honey.AddComponent(sugar);
                    honey.Operation();
                }
                else
                {
                    Console.WriteLine("Invalid coffee ingredients");
                }
            }
            else
            {
                Console.WriteLine("Invalid coffee option");
            }
        }
    }
}