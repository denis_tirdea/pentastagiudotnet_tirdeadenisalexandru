﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    class ConcreteHTMLPage : Server
    {
        HTMLPage htmlPage = new HTMLPage();
        Context context = new Context(new ConcreteStrategy());

        public override void ReadHtmlPage(string Page)
        {
            try
            {
                System.Net.WebClient webClient = new System.Net.WebClient();
                string webData = webClient.DownloadString(Page);
                htmlPage.Add(webData);
                context.ContextAlgorithm(webData);
                Console.WriteLine("HTML page processed");
            }
            catch
            {
                Console.WriteLine("We couldn't process your web page");
            }
        }

        public override HTMLPage ShowHtmlPage()
        {
            return htmlPage;
        }
    }
}