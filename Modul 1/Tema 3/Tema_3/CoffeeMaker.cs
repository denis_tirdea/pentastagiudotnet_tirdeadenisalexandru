﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    public class Coffee : Component
    {
        private int _coffeeID;
        private double[] _coffeePrices = new double[6] { 0, 1, 1.25, 2, 2.5, 5 };

        public Coffee(int coffeeID)
        {
            this._coffeeID = coffeeID;
        }

        public override void Operation()
        {
            Console.WriteLine("Selected coffee: {0} price : {1}$", (CoffeeMaker)_coffeeID, _coffeePrices[_coffeeID]);
            Console.WriteLine("With ingredients: ");
        }
    }

    abstract public class Decorator : Component
    {
        protected Component component;

        public void AddComponent(Component component)
        {
            this.component = component;
        }

        public override void Operation()
        {
            if (component != null)
            {
                component.Operation();
            }
        }
    }

    abstract public class Component
    {
        abstract public void Operation();
    }

    public class Sugar : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            Console.Write("Sugar; ");
        }
    }

    public class Milk : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            Console.Write("Milk; ");
        }
    }

    public class Honey : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            Console.Write("Honey; ");
        }
    }
}