﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    public enum Command
    {
        Exit,
        DownloadNetPages,
        ProcessInputNumbers,
        CoffeeMaker,
        ReadOnePage = 1,
        ReadMorePages
    }

    public enum CoffeeMaker
    {
        Exit,
        Espresso,
        EspressoItaliano,
        Cappuccino,
        Moccacino,
        ClassicCoffee
    }
}