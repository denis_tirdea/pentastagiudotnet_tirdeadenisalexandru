﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema_3
{
    class FirstImplementation : Processor
    {
        public override void FirstStep()
        {
            inputValue += 2;
        }

        public override void SecondStep()
        {
            inputValue *= 4;
        }

        public override void ThirdStep()
        {
            Console.WriteLine("Your new number: " + inputValue);
        }
    }
}